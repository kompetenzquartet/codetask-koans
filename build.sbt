ThisBuild / version := "3.0"

ThisBuild / scalaVersion := "3.1.0"

lazy val root = (project in file("."))
  .settings(
    name := "de.htwg.scala.koans"
  )

// Current latest v.3.2.15 official supports just Scala v.3.1.0
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.15" % "test"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.15"
// https://typelevel.org/spire/
libraryDependencies += "org.typelevel" %% "spire" % "0.18.0"

libraryDependencies += "junit" % "junit" % "4.13.2" % "test"
libraryDependencies += "com.h2database" % "h2" % "2.1.214"
libraryDependencies += "org.slf4j" % "slf4j-nop" % "2.0.6"

// Akka: https://doc.akka.io/docs/akka/current/index.html
// Scala 3 is not full supportet: https://doc.akka.io/docs/akka/current/project/scala3.html
val AkkaVersion = "2.7.0"
val AkkaHttpVersion = "10.5.0"
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  // Akka-http: https://doc.akka.io/docs/akka-http/current/
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion

// Slick: https://scala-slick.org/
// Aktuellste version ist nicht kompatible mit scala 3, Src.: https://scala-slick.org/doc/prerelease/upgrade.html, https://github.com/playframework/play-slick
// TODO: Alternative Bibliothek sind Quill oder Doobie.
//  "com.typesafe.slick" %% "slick" % "3.4.1",
//  "com.typesafe.slick" %% "slick-hikaricp" % "3.4.1"
)
