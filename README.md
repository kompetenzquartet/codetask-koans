# Scala Koans for Lectures at HTWG

## To create a course, please follow these steps.

1. you use the source code from below to create an example of each task type and use them as template or Search in code.
2. There is a meta.json file sample to. It declares the course. You can copy it for creation.
3. Then you have to push it to the master, and subsequently, request the data from the Codetask Admin interface.
   (https:codetask.in.htwg-konstanz.de/admin -> Koans/"Koans synchronisieren")

**Template Sample: meta.json**

{
"id": 1720436776750,
"title": "Exercises Tutorial",
"description": "Master your tasks with ease: A step-by-step guide to conquering Task Types in Codetask!"
}

**Template Sample: Sample course**

import org.codetask.koanlib.CodeTaskSuite

class Ex01_TaskTypes extends CodeTaskSuite("The 7 Task-Types", 1) {

info(
"""# Info Task
It is recommended to follow the instructions provided to enhance your learning experience. To solve this, just klick on _'Als Erledigt markieren!'_"""
)
reflection(
"""# Reflection Task
There is no right or wrong answer. This task is designed to encourage thoughtful consideration and to compose a clear response.
If you click on _'Speichern'_ the Task is solved and the written answer can't be changed. Try it out:

     *Example*
     What do you expect to learn on Codetask?"""

)
video(
"""# Video Task
Autoplay is enabled. To disable autoplay, click on your name in the upper right corner, then click on 'Settings'. It should be the first option.
To solve this, you have to watch the video until the end.

     *Example*
     in this case, feel free to _skip_ to end""",
     "dQw4w9WgXcQ"

)
singleChoice(
"""# Single Choice Task
Only one answer is correct. If you fail, don't worry and try it again!

     *Example*
     What colour is the sky?
     """

) {
val solutions = List(
("White", false),
("Blue", true),
("Yellow", false),
("None", false)
)
}
multipleChoice(
"""# Multiple Choice Task
Several answers could be correct. If you fail, don't worry and try it again!

     *Example*
     Which of these colours are present in the rainbow?
     """

) {
val solutions = List(
("Black", false),
("Blue", true),
("Red", true),
("Grey", false)
)
}
info(
"""Now that you've finished, if you feel confident with handling the task types, you can also disable the additional task information displayed in the light gray box below the task prompt. You can find this option in the Settings as well. \n \n # Good luck and have fun, with your learning!"""
)
}
