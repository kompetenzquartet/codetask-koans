package de.htwg.Tutorial
import org.codetask.koanlib.CodeTaskSuite

class Ex01_ResponsibleUsage extends CodeTaskSuite("Responsible Usage of GitHub Copilot", 1) {

  info(
    """# Info Task
    GitHub Copilot is an AI-powered code completion tool that helps developers by suggesting code snippets and entire functions. It is important to use it responsibly to ensure code quality and security.
    """
  )

  reflection(
    """# Reflection Task
    Reflect on how you plan to use GitHub Copilot in your development workflow. What steps will you take to ensure that the code suggestions are secure and of high quality?
    """
  )

  video(
    """# Video Task
    Watch this video to understand the features and capabilities of GitHub Copilot. Make sure to watch until the end to complete the task.
    """,
    "jXp5D5ZnxGM"
  )

  singleChoice(
    """# Single Choice Task
    What is one of the key benefits of using GitHub Copilot?
    """
  ) {
    val solutions = List(
      ("It writes code for you without any input", false),
      ("It suggests code snippets based on the context", true),
      ("It replaces the need for human developers", false),
      ("It only works with JavaScript", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following are best practices when using GitHub Copilot?
    """
  ) {
    val solutions = List(
      ("Review the suggested code for security vulnerabilities", true),
      ("Blindly accept all suggestions", false),
      ("Use it to learn new coding techniques", true),
      ("Ignore the context of the code", false)
    )
  }

  info(
    """# Info Task
    GitHub Copilot can help you write code faster, but it is important to review and test the code to ensure it meets your project's standards.
    """
  )

  singleChoice(
    """# Single Choice Task
    What should you do if GitHub Copilot suggests code that you do not understand?
    """
  ) {
    val solutions = List(
      ("Use it anyway", false),
      ("Ignore it and write your own code", true),
      ("Ask a colleague to review it", true),
      ("Assume it is correct", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    How can you ensure the code suggested by GitHub Copilot is secure?
    """
  ) {
    val solutions = List(
      ("Review the code for potential vulnerabilities", true),
      ("Run security tests on the code", true),
      ("Assume all suggestions are secure", false),
      ("Use it without any modifications", false)
    )
  }

  video("# Recursive Fizz Buzz", "5sryc6JUe6M")
  codetask(
    """ Exercise: (Complex Number Multiplication)
    Write a function that multiplies two complex numbers. A complex number is represented as a pair of integers (a, b) where 'a' is the real part and 'b' is the imaginary part.

    The multiplication of two complex numbers (a + bi) and (c + di) is given by:
    (a + bi) * (c + di) = (ac - bd) + (ad + bc)i

    Use GitHub Copilot to assist you in writing this function.""") {
    case class ComplexNumber(real: Int, imaginary: Int)

    def multiplyComplexNumbers(c1: ComplexNumber, c2: ComplexNumber): ComplexNumber = {
      //solve
      val realPart = c1.real * c2.real - c1.imaginary * c2.imaginary
      val imaginaryPart = c1.real * c2.imaginary + c1.imaginary * c2.real
      ComplexNumber(realPart, imaginaryPart)
      //endsolve
    }

    //test
    val c1 = ComplexNumber(1, 2)
    val c2 = ComplexNumber(3, 4)
    val result = multiplyComplexNumbers(c1, c2)
    result.real should be(-5)
    result.imaginary should be(10)
    //endtest
  }

  info(
    """# Info Task
    Remember to always review and test the code suggested by GitHub Copilot to ensure it meets your project's requirements and standards.
    """
  )
}
