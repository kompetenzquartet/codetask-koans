package de.htwg.Tutorial
import org.codetask.koanlib.CodeTaskSuite

class Ex06_ScalaSetupGeneric extends CodeTaskSuite("Setting Up Scala Without an IDE", 1) {

  info(
    """# Info Task
    Setting up a Scala development environment without an IDE involves installing Scala and SBT, and using a text editor and terminal for development.
    """
  )

  info(
    """# Info Task
    Follow these steps to set up Scala and SBT:
    1. Download and install Scala from the [official website](https://www.scala-lang.org/download/).
    2. Download and install SBT from the [official website](https://www.scala-sbt.org/download.html).
    3. Verify the installation by running `scala -version` and `sbt -version` in the terminal.
    """
  )

  singleChoice(
    """# Single Choice Task
    Which command verifies the installation of Scala?
    """
  ) {
    val solutions = List(
      ("scala -version", true),
      ("sbt -version", false),
      ("java -version", false),
      ("scalac -version", false)
    )
  }

  info(
    """# Info Task
    To create a new Scala project using SBT:
    1. Open a terminal and navigate to the directory where you want to create the project.
    2. Run `sbt new scala/scala-seed.g8` to create a new Scala project from a template.
    3. Follow the prompts to configure the project settings (e.g., project name, organization).
    4. Navigate to the project directory and run `sbt compile` to compile the project.
    """
  )

  singleChoice(
    """# Single Choice Task
    Which command creates a new Scala project using SBT?
    """
  ) {
    val solutions = List(
      ("sbt new scala/scala-seed.g8", true),
      ("sbt init", false),
      ("sbt create", false),
      ("sbt start", false)
    )
  }

  info(
    """# Info Task
    You can use any text editor to write Scala code. Popular choices include Visual Studio Code, Sublime Text, and Atom. Make sure to install the Scala plugin for your chosen editor to enable syntax highlighting and other features.
    """
  )

  multipleChoice(
    """# Multiple Choice Task
    Which of the following text editors can be used to write Scala code?
    """
  ) {
    val solutions = List(
      ("Visual Studio Code", true),
      ("Sublime Text", true),
      ("Atom", true),
      ("Microsoft Word", false)
    )
  }

  info(
    """# Info Task
    To run your Scala code, use the SBT shell:
    1. Open a terminal and navigate to your project directory.
    2. Run `sbt` to start the SBT shell.
    3. Enter `run` to compile and run your Scala code.
    """
  )

  singleChoice(
    """# Single Choice Task
    Which command starts the SBT shell?
    """
  ) {
    val solutions = List(
      ("sbt", true),
      ("scala", false),
      ("run", false),
      ("compile", false)
    )
  }

  info(
    """# Info Task
    Remember to always review and test your code to ensure it meets your project's requirements and standards.
    """
  )
}
