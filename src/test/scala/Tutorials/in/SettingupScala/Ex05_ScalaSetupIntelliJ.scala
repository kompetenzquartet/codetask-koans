package de.htwg.Tutorial
import org.codetask.koanlib.CodeTaskSuite

class Ex05_ScalaSetupIntelliJ extends CodeTaskSuite("Setting Up Scala with IntelliJ IDEA", 2) {

  info(
    """# Info Task
    IntelliJ IDEA is a powerful IDE that supports Scala development. It provides features like intelligent code completion, integrated SBT shell, and debugging tools.
    """
  )

  info(
    """# Info Task
    Students can get the Ultimate version of IntelliJ IDEA for free. [Get IntelliJ IDEA Ultimate for free](https://www.jetbrains.com/community/education/#students)
    """
  )

  info(
    """# Info Task
    Follow these steps to set up a Scala project in IntelliJ IDEA:
    1. Download and install IntelliJ IDEA from the [official website](https://www.jetbrains.com/idea/download/).
    2. Open IntelliJ IDEA and click on "New Project".
    3. Select "Scala" from the list of project types.
    4. Choose "SBT" as the build tool and configure the project settings (e.g., project name, location).
    5. Click "Finish" to create the project.
    """
  )

  singleChoice(
    """# Single Choice Task
    Which build tool should you select when creating a Scala project in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Maven", false),
      ("Gradle", false),
      ("SBT", true),
      ("Ant", false)
    )
  }

  info(
    """# Info Task
    IntelliJ IDEA provides an integrated SBT shell that allows you to run SBT commands directly from the IDE. This makes it easy to manage dependencies, compile code, and run tests.
    """
  )

  reflection(
    """# Reflection Task
    Reflect on how the integrated SBT shell in IntelliJ IDEA can improve your development workflow. What benefits do you see in using it?
    """
  )

  info(
    """# Info Task
    To use the integrated SBT shell in IntelliJ IDEA:
    1. Open the "SBT" tool window.
    2. Enter SBT commands (e.g., `compile`, `test`) in the shell.
    3. View the output and results directly in the IDE.
    """
  )

  singleChoice(
    """# Single Choice Task
    Where can you find the integrated SBT shell in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("File > Settings", false),
      ("View > Tool Windows > SBT", true),
      ("Help > Find Action", false),
      ("Run > Edit Configurations", false)
    )
  }

  info(
    """# Info Task
    IntelliJ IDEA also provides debugging tools that allow you to set breakpoints, step through code, and inspect variables. This can help you identify and fix issues in your Scala code.
    """
  )

  singleChoice(
    """# Single Choice Task
    What is the purpose of setting a breakpoint in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("To pause the execution of the program at a specific point", true),
      ("To automatically fix errors in the code", false),
      ("To generate code snippets", false),
      ("To compile the code", false)
    )
  }

  info(
    """# Info Task
    Remember to always review and test your code to ensure it meets your project's requirements and standards.
    """
  )
}
