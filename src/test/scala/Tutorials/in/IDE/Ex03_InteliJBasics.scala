package de.htwg.Tutorial
import org.codetask.koanlib.CodeTaskSuite

class Ex03_IntelliJBasics extends CodeTaskSuite("Introduction to IntelliJ IDEA", 1) {

  info(
    """# Info Task
    IntelliJ IDEA is a powerful integrated development environment (IDE) for Java, Scala, and many other languages. It provides a wide range of features to help you write, debug, and test your code efficiently.
    """
  )

  info(
    """# Info Task
    Students can get the Ultimate version of IntelliJ IDEA for free. [Get IntelliJ IDEA Ultimate for free](<https://www.jetbrains.com/community/education/#students>)
    """
  )

  reflection(
    """# Reflection Task
    Reflect on how you plan to use IntelliJ IDEA in your development workflow. What features do you find most useful and why?
    """
  )

  video(
    """# Video Task
    Watch this video to understand the basics of IntelliJ IDEA and how to use it effectively. Make sure to watch until the end to complete the task.
    """,
    "70_B2DyM8mU"
  )

  singleChoice(
    """# Single Choice Task
    What is one of the key benefits of using IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("It provides intelligent code completion", true),
      ("It automatically writes code for you", false),
      ("It replaces the need for human developers", false),
      ("It only works with JavaScript", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following languages are supported by IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Java", true),
      ("Scala", true),
      ("Python", true),
      ("HTML", true)
    )
  }

  info(
    """# Info Task
    Here are some basic steps to create a new project in IntelliJ IDEA:
    1. Open IntelliJ IDEA and click on "New Project".
    2. Select the project type (e.g., Java, Scala).
    3. Configure the project settings (e.g., project name, location).
    4. Click "Finish" to create the project.
    """
  )

  singleChoice(
    """# Single Choice Task
    Which option do you select to create a new project in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("New Project", true),
      ("Open Project", false),
      ("Import Project", false),
      ("Clone Project", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following features are available in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Code completion", true),
      ("Version control integration", true),
      ("Debugging tools", true),
      ("Automatic code generation", false)
    )
  }

  info(
    """# Info Task
    IntelliJ IDEA provides powerful debugging tools to help you find and fix issues in your code. You can set breakpoints, step through code, and inspect variables.
    """
  )

  singleChoice(
    """# Single Choice Task
    What is the purpose of setting a breakpoint in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("To pause the execution of the program at a specific point", true),
      ("To automatically fix errors in the code", false),
      ("To generate code snippets", false),
      ("To compile the code", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following actions can you perform while debugging in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Step over", true),
      ("Step into", true),
      ("Resume program", true),
      ("Delete breakpoints", true)
    )
  }

  info(
    """# Info Task
    IntelliJ IDEA integrates with version control systems like Git, allowing you to manage your codebase and collaborate with others directly from the IDE.
    """
  )

  singleChoice(
    """# Single Choice Task
    Which version control system is integrated with IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Git", true),
      ("SVN", true),
      ("Mercurial", true),
      ("All of the above", true)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following actions can you perform with Git integration in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Commit changes", true),
      ("Push changes", true),
      ("Pull changes", true),
      ("Merge branches", true)
    )
  }

  info(
    """# Info Task
    IntelliJ IDEA provides a wide range of plugins to extend its functionality. You can install plugins for additional languages, frameworks, and tools.
    """
  )

  singleChoice(
    """# Single Choice Task
    Where can you find and install plugins in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("File > Settings > Plugins", true),
      ("File > New Project", false),
      ("File > Open Project", false),
      ("File > Import Project", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following plugins are available for IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Scala", true),
      ("Python", true),
      ("Kotlin", true),
      ("Ruby", true)
    )
  }

  info(
    """# Info Task
    Remember to always review and test your code to ensure it meets your project's requirements and standards.
    """
  )
}
