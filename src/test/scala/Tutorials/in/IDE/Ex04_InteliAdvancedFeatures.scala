package de.htwg.Tutorial
import org.codetask.koanlib.CodeTaskSuite

class Ex04_IntelliJAdvancedFeatures extends CodeTaskSuite("Advanced Features of IntelliJ IDEA", 2) {

  info(
    """# Info Task
    IntelliJ IDEA offers advanced features that can significantly enhance your productivity and code quality. These features include code refactoring, code analysis, and integration with build tools.
    """
  )

  info(
    """# Info Task
    Code refactoring allows you to restructure your code without changing its external behavior. IntelliJ IDEA provides various refactoring options such as renaming, extracting methods, and changing method signatures.
    """
  )

  singleChoice(
    """# Single Choice Task
    Which of the following is an example of a code refactoring operation in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Renaming a variable", true),
      ("Running unit tests", false),
      ("Committing changes to Git", false),
      ("Formatting code", false)
    )
  }

  info(
    """# Info Task
    Code analysis helps you identify potential issues in your code. IntelliJ IDEA provides real-time code analysis and suggests improvements to enhance code quality.
    """
  )

  multipleChoice(
    """# Multiple Choice Task
    Which of the following are benefits of using code analysis in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Identifying potential bugs", true),
      ("Improving code readability", true),
      ("Automatically fixing all issues", false),
      ("Ensuring code follows best practices", true)
    )
  }

  info(
    """# Info Task
    IntelliJ IDEA integrates with various build tools such as Maven, Gradle, and SBT. This integration allows you to manage dependencies, run build tasks, and automate your build process directly from the IDE.
    """
  )

  singleChoice(
    """# Single Choice Task
    Which build tool is commonly used for Scala projects and is supported by IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Maven", false),
      ("Gradle", false),
      ("SBT", true),
      ("Ant", false)
    )
  }

  info(
    """# Info Task
    IntelliJ IDEA provides powerful version control integration, allowing you to manage your codebase and collaborate with others. You can perform actions such as committing changes, creating branches, and resolving conflicts directly from the IDE.
    """
  )

  reflection(
    """# Reflection Task
    Reflect on how you plan to use the advanced features of IntelliJ IDEA in your development workflow. Which features do you find most useful and why?
    """
  )

  info(
    """# Info Task
    Let's practice using some advanced features. Follow these steps to refactor a method in your project:
    1. Open a Scala file in your project.
    2. Select a method you want to refactor.
    3. Right-click and choose "Refactor" > "Rename".
    4. Enter a new name for the method and press Enter.
    """
  )

  singleChoice(
    """# Single Choice Task
    What is the purpose of renaming a method in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("To change the method's functionality", false),
      ("To improve code readability", true),
      ("To delete the method", false),
      ("To add a new method", false)
    )
  }

  info(
    """# Info Task
    Now, let's practice using code analysis. Follow these steps to analyze your code:
    1. Open a Scala file in your project.
    2. Click on "Code" > "Inspect Code".
    3. Review the analysis results and apply suggested improvements.
    """
  )

  multipleChoice(
    """# Multiple Choice Task
    Which of the following actions can you perform based on the code analysis results in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("Fix potential bugs", true),
      ("Improve code readability", true),
      ("Automatically commit changes", false),
      ("Refactor code", true)
    )
  }

  info(
    """# Info Task
    Finally, let's practice using build tool integration. Follow these steps to run a build task:
    1. Open the "Build" tool window.
    2. Select a build task (e.g., "compile").
    3. Click the "Run" button to execute the task.
    """
  )

  singleChoice(
    """# Single Choice Task
    What is the purpose of running a build task in IntelliJ IDEA?
    """
  ) {
    val solutions = List(
      ("To compile the code", true),
      ("To delete the project", false),
      ("To rename a method", false),
      ("To format the code", false)
    )
  }

  info(
    """# Info Task
    Remember to always review and test your code to ensure it meets your project's requirements and standards.
    """
  )
}
