package de.htwg.Tutorial
import org.codetask.koanlib.CodeTaskSuite

class Ex02_GitBasics extends CodeTaskSuite("Introduction to Git", 1) {

  info(
    """# Info Task
    Git is a distributed version control system that helps developers track changes in their codebase, collaborate with others, and manage their projects efficiently.
    """
  )

  reflection(
    """# Reflection Task
    Reflect on how you plan to use Git in your development workflow. What steps will you take to ensure that your commits are meaningful and well-documented?
    """
  )

  video(
    """# Video Task
    Watch this video to understand the basics of Git and how to use it effectively. Make sure to watch until the end to complete the task.
    """,
    "HkdAHXoRtos"
  )

  singleChoice(
    """# Single Choice Task
    What is one of the key benefits of using Git?
    """
  ) {
    val solutions = List(
      ("It allows you to track changes in your codebase", true),
      ("It automatically writes code for you", false),
      ("It replaces the need for human developers", false),
      ("It only works with JavaScript", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following are platforms that support Git repositories?
    """
  ) {
    val solutions = List(
      ("GitHub", true),
      ("GitLab", true),
      ("Bitbucket", true),
      ("Google Drive", false)
    )
  }

  info(
    """# Info Task
    Git can help you manage your codebase and collaborate with others, but it is important to understand its basic commands and workflows.
    """
  )

  singleChoice(
    """# Single Choice Task
    What should you do if you encounter a merge conflict in Git?
    """
  ) {
    val solutions = List(
      ("Ignore it and push your changes", false),
      ("Resolve the conflict manually and commit the changes", true),
      ("Delete the conflicting files", false),
      ("Revert to an earlier commit", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    How can you ensure that your Git commits are meaningful and well-documented?
    """
  ) {
    val solutions = List(
      ("Write clear and concise commit messages", true),
      ("Commit frequently with small changes", true),
      ("Include detailed descriptions of the changes", true),
      ("Commit large changes infrequently", false)
    )
  }

  info(
    """# Info Task
    Here are some basic Git commands and their usage:
    - `git init`: Initialize a new Git repository.
    - `git clone <repository_url>`: Clone an existing repository.
    - `git add <file>`: Stage changes for commit.
    - `git commit -m "message"`: Commit staged changes with a message.
    - `git push`: Push commits to a remote repository.
    - `git pull`: Fetch and merge changes from a remote repository.
    - `git status`: Show the working tree status.
    - `git log`: Show commit logs.
    """
  )

  singleChoice(
    """# Single Choice Task
    Which command is used to initialize a new Git repository?
    """
  ) {
    val solutions = List(
      ("git init", true),
      ("git clone", false),
      ("git add", false),
      ("git commit", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following commands are used to stage and commit changes in Git?
    """
  ) {
    val solutions = List(
      ("git add", true),
      ("git commit", true),
      ("git push", false),
      ("git pull", false)
    )
  }

  info(
    """# Info Task
    Remember to always review and test your code before committing to ensure it meets your project's requirements and standards.
    """
  )
}
