package Tutorials.in.test

import org.codetask.koanlib.CodeTaskSuite
import java.io.{ByteArrayOutputStream, PrintStream}

class Ex01_test extends CodeTaskSuite("Tests", 1) {

  codetask(
    """Test if something was printed with println"""
  ) {
    // Define a method that prints a message
    def printMessage(message: String): Unit = {
      //solve
      println(message)
      //endsolve
    }

    // Capture the original System.out
    val originalOut = System.out

    // Create a stream to capture output
    val outputStream = new ByteArrayOutputStream()
    val printStream = new PrintStream(outputStream)

    // Redirect System.out to our stream
    System.setOut(printStream)

    //test
    val testMessage = "Hello, World!"
    printMessage(testMessage)
    System.out.flush()
    System.setOut(originalOut)

    // Convert captured output to string and verify
    val printedOutput = outputStream.toString.trim
    printedOutput should be(testMessage)
    //endtest
  }

info(
  """# Info Task


This is an info task where I want to show multiple different formatting.

## Subheading Level 2

### Subheading Level 3

Here is a blockquote:
<blockquote style="color: blue;">This is a blockquote example.</blockquote>

Here is a table:
<table style="border: 1px solid black;">
  <tr>
    <th style="background-color: lightgray;">Header 1</th>
    <th style="background-color: lightgray;">Header 2</th>
  </tr>
  <tr>
    <td style="color: red;">Row 1</td>
    <td style="color: green;">Data 1</td>
  </tr>
  <tr>
    <td style="color: red;">Row 2</td>
    <td style="color: green;">Data 2</td>
  </tr>
</table>

Here is some Scala code:
<pre style="background-color: #f5f5f5; padding: 10px; border-radius: 4px;"><code class="language-scala">
val example = "This is a Scala code block"
println(example)

</code></pre>


"""
  )
}
