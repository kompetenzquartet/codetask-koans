package art.of.programming

import org.codetask.koanlib.CodeTaskSuite

class Ch08_Loops extends CodeTaskSuite("Types and Arguments", 7) {
info("""# Scala 3 introduces a new syntax for control structures including loops. 
  This learning section will help youself guiding through the changes and how to use them.
  """)

singleChoice(
  """# What is the correct syntax for a loop to print each element in a list?
  
   Hint: Use the [docs](https://docs.scala-lang.org/scala3/book/taste-control-structures.html)."""
) {
  val solutions = List(
    ("for (i <- list) println(i)", false),
    ("for i <- list do println(i)", true),
    ("for i <- list println(i)", false),
    ("for (i <- list) do println(i)", false)
  )
}

multipleChoice(
  """# Which of the following are valid control structures? 
  
  Hint: Use the [docs](https://docs.scala-lang.org/scala3/book/taste-control-structures.html).
  """
) {
  val solutions = List(
    ("if/else", true),
    ("for loops and expressions", true),
    ("match expressions", true),
    ("try/catch/finally", true),
    ("while loops", true),
    ("do/while loops", false)
  )
}

reflection(
  """# What do you think about this section and how do you find it to work out solutions with the documentation?"""
)

}