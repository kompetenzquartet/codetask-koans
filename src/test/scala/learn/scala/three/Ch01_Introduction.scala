package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch01_Introduction extends CodeTaskSuite("Scala Basics", 1) {

  koan(
    """# Welcome to this course for the Introduction on Scala 3 Basics, it's based on the Book "Scala Cookbook Recipes for Object-Oriented and Functional Programming" by Alvin Alexander
        as well as the Introduction to Scala 3 online course by Alvin Alexander. This course combines Info tasks as well as Videos and interactive Parts where you can Answer Questions as well as Code
        Tasks, or Koans. You should read the Information or watch the Video and then based on that Answer the Questions.
      """
  ) {
    2 + 3 should be(5)
  }

  info(
    """# Info Task
       Ok to start on this course I recommend having the Scala live Code editor open as well. You can find it on
       the left side under Scala IDE, it can be very helpful in finding and testing solutions and some Tasks will encourage you
       to try solutions there first and then Answer questions on that. You can open that in a new tab or switch there when wanted.
       Some tasks will require you to fill in the blanks in the code that follows, or write a function yourself.
        """
  )
  

  info(
    """# Info Task
       So let's start with a Simple Task. In Scala, the println method can be used to write to the console, and in typical Programming manner, I would like you to print something to the
       console. So please write a println statement that prints "Hello World" to the console.
        """
  )
  
  info(
    """# Info Task
       In Scala, there are two types of variables: `val` and `var`.

       - `val` is used to declare an immutable variable, meaning once it is assigned a value, it cannot be changed. It is similar to a final variable in Java.
       - `var` is used to declare a mutable variable, meaning its value can be changed after it is assigned.

       Example:
       ```scala
       val immutableValue = 10
       // immutableValue = 20 // This will cause a compilation error

       var mutableValue = 10
       mutableValue = 20 // This is allowed
       ```

       It is recommended to use `val` whenever possible to ensure immutability, which can help prevent bugs and make your code easier to reason about.
       You can watch the following video if you want a more indepth explanation on this topic.
     """
  )

  koan(
    """# Val and Var:
    Test your knowledge on `val` and `var` by trying out some basic operations and assignments.
    """) {
    val immutableValue = 10
    var mutableValue = 10

    // Immutable value cannot be reassigned
    // Uncommenting the following line should cause a compilation error
    // immutableValue = 20

    // Mutable value can be reassigned
    mutableValue = 20
    mutableValue should be(20)

    // Immutable value remains unchanged
    immutableValue should be(10)

    // Mutable value can be incremented
    mutableValue += 5
    mutableValue should be(25)
  }
  
  singleChoice(
    """# Single Choice Task
    Which of the following statements about `val` and `var` is true?
    """
  ) {
    val solutions = List(
      ("A `val` can be reassigned after its initial assignment", false),
      ("A `var` can be reassigned after its initial assignment", true),
      ("Both `val` and `var` can be reassigned after their initial assignment", false),
      ("Neither `val` nor `var` can be reassigned after their initial assignment", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following statements about `val` and `var` are true? (Select all that apply)
    """
  ) {
    val solutions = List(
      ("A `val` is immutable", true),
      ("A `var` is mutable", true),
      ("A `val` can be reassigned", false),
      ("A `var` cannot be reassigned", false)
    )
  }
}
  
