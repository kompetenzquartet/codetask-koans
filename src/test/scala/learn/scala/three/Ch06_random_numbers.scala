package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite
import scala.util.Random

class Ch06_random_numbers extends CodeTaskSuite("Random Numbers", 6) {
  info(
"""# Info Task

## Generating Random Numbers

Scala provides the `scala.util.Random` class to generate random numbers. Here are some examples:

```scala
val random = new Random()
val randomInt = random.nextInt(100)  // Generates a random integer between 0 and 99
val randomDouble = random.nextDouble()  // Generates a random double between 0.0 and 1.0
```
"""
  )

  koan(
"""# Random Number Range Tests
Test if a random number is within a specified range.
"""
  ) {
    val random = new Random()
    val randomInt = random.nextInt(100)

    // Check if the random number is within the range 0 to 99
    (randomInt >= 0 && randomInt < 100) should be(true)

    // Check if the random number is within the range 100 to 200 (this should be false)
    (randomInt >= 100 && randomInt < 200) should be(false)
  }

  koan(
"""# Generate a Random Number
Write a function that generates a random integer between 0 and a given upper bound (exclusive).
"""
  ) {
    def generateRandomNumber(upperBound: Int): Int = {
      val random = new Random()
      random.nextInt(upperBound)
    }

    val upperBound = 50
    val randomNumber = generateRandomNumber(upperBound)
    (randomNumber >= 0 && randomNumber < upperBound) should be(true)
  }

  singleChoice(
"""# Single Choice Task
What is the range of values that `random.nextInt(100)` can generate?
"""
  ) {
    val solutions = List(
      ("0 to 99", true),
      ("1 to 100", false),
      ("0 to 100", false),
      ("1 to 99", false)
    )
  }

  multipleChoice(
"""# Multiple Choice Task
Which of the following methods can be used to generate random numbers in Scala?
"""
  ) {
    val solutions = List(
      ("random.nextInt()", true),
      ("random.nextDouble()", true),
      ("random.nextString()", false),
      ("random.nextBoolean()", true)
    )
  }
}