package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch02_numeric_types extends CodeTaskSuite("Scala Numeric Types", 2) {
  info(
    """# Info Task

  ## Numeric Types in Scala

  Scala supports various numeric types, each with its own range and precision. Here is a table summarizing some of the common numeric types:

  <table style="border: 1px solid black;">
    <tr>
      <th style="background-color: lightgray;">Data type</th>
      <th style="background-color: lightgray;">Description</th>
      <th style="background-color: lightgray;">Range</th>
    </tr>
    <tr>
      <td style="color: red;">Char</td>
      <td style="color: green;">16-bit unsigned Unicode character</td>
      <td style="color: green;">0 to 65,535</td>
    </tr>
    <tr>
      <td style="color: red;">Byte</td>
      <td style="color: green;">8-bit signed value</td>
      <td style="color: green;">–128 to 127</td>
    </tr>
    <tr>
      <td style="color: red;">Short</td>
      <td style="color: green;">16-bit signed value</td>
      <td style="color: green;">–32,768 to 32,767</td>
    </tr>
    <tr>
      <td style="color: red;">Int</td>
      <td style="color: green;">32-bit signed value</td>
      <td style="color: green;">–2,147,483,648 to 2,147,483,647</td>
    </tr>
    <tr>
      <td style="color: red;">Long</td>
      <td style="color: green;">64-bit signed value</td>
      <td style="color: green;">–2^63 to 2^63–1, inclusive</td>
    </tr>
    <tr>
      <td style="color: red;">Float</td>
      <td style="color: green;">32-bit IEEE 754 single precision float</td>
      <td style="color: green;">See below</td>
    </tr>
    <tr>
      <td style="color: red;">Double</td>
      <td style="color: green;">64-bit IEEE 754 double precision float</td>
      <td style="color: green;">See below</td>
    </tr>
  </table>
  """
  )
  koan(
  """# Data Type Comparisons
  Compare the following data types using smaller and bigger comparisons. Write `true` or `false` in the field to indicate whether the comparison is correct.
  """
) {
  val charValue: Char = Char.MaxValue
  val byteValue: Byte = Byte.MaxValue
  val shortValue: Short = Short.MaxValue
  val intValue: Int = Int.MaxValue
  val longValue: Long = Long.MaxValue
  val floatValue: Float = Float.MaxValue
  val doubleValue: Double = Double.MaxValue

  charValue < byteValue should be(false)

  shortValue < intValue should be(true)

  intValue < longValue should be(true)

  floatValue < doubleValue should be(true)

  byteValue < shortValue should be(true)

  longValue < doubleValue should be(false)
}
  koan(
    """# Instance Of Tests
    Test the instance of different numeric types. Some tests should be true and some should be false.
    """
  ) {
    // Int
    val int1 = 1_000
    val int2 = 100_000

    int1.isInstanceOf[Int] should be(true)
    int2.isInstanceOf[Int] should be(true)

    // Long
    val long1 = 1_000_000L

    long1.isInstanceOf[Long] should be(true)
    long1.isInstanceOf[Int] should be(false)

    // Double
    val double1 = 1_123.45

    double1.isInstanceOf[Double] should be(true)
    double1.isInstanceOf[Float] should be(false)

    // Float
    val float1 = 3_456.7F

    float1.isInstanceOf[Float] should be(true)
    float1.isInstanceOf[Double] should be(false)

    // BigInt
    val bigInt: BigInt = 1_000_000

    bigInt.isInstanceOf[BigInt] should be(true)
    bigInt.isInstanceOf[Int] should be(false)
  }



}

