package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch11_StringInterpolators extends CodeTaskSuite("String Interpolators in Scala", 11) {

  info(
    """# Info Task

  String interpolators in Scala are used to embed variables and expressions within strings. They provide a more readable and concise way to create strings compared to using the `+` operator.

  ### Types of String Interpolators

  ** `s` Interpolator **
  The simplest interpolator, used to embed variables and expressions.

  ** `f` Interpolator **
  Used for formatted strings, similar to `printf` in other languages.

  ** `raw` Interpolator **
  Used to create raw strings, where escape characters are not processed.


  <blockquote style="color: blue;">Example of `s` interpolator:</blockquote>

  <table style="border: 1px solid black;">
    <tr>
      <th style="background-color: lightgray;">Description</th>
      <th style="background-color: lightgray;">Code</th>
    </tr>
    <tr>
      <td style="color: red;">Embedding variables</td>
      <td style="color: green;">val description = s"$university offers a course in $course."</td>
    </tr>
    <tr>
      <td style="color: red;">Embedding expressions</td>
      <td style="color: green;">val result = s"${2 * 3} students passed the exam."</td>
    </tr>
    <tr>
      <td style="color: red;">Using case class</td>
      <td style="color: green;">val studentDescription = s"${student.firstName} ${student.lastName} is enrolled in the course."</td>
    </tr>
  </table>

  <pre style="background-color: #f5f5f5; padding: 10px; border-radius: 4px;"><code class="language-scala">
  val university = "HTWG Konstanz"
  val course = "Computer Science"
  val description = s"$university offers a course in $course."

  val result = s"${2 * 3} students passed the exam."

  case class Student(firstName: String, lastName: String)
  val student = Student("John", "Doe")
  val studentDescription = s"${student.firstName} ${student.lastName} is enrolled in the course."
  </code></pre>

  """
  )
  video("# String Interpolators", "d60fdxmOKXU")

  codetask(
    """# Code Task
    Create a function that takes a `Car` object and returns a description message using string interpolation.
    """
  ) {
    case class Car(make: String, model: String)

    def describe(car: Car): String = {
      //solve
      s"The car is a ${car.make} ${car.model}."
      //endsolve
    }

    //test
    val car = Car("Volkswagen", "Golf 7")
    val description = describe(car)
    description should be("The car is a Volkswagen Golf 7.")
    //endtest
  }
}