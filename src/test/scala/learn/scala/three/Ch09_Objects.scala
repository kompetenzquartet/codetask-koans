package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch09_Objects extends CodeTaskSuite("Objects in Scala", 9) {

  info(
    """# Objects in Scala
    In Scala, objects are single instances of their own definitions. They are similar to classes, but they cannot be instantiated multiple times. Objects are used for defining singletons, utility methods, and constants.

    Let's explore objects in Scala with examples.
    """
  )

  koan(
    """# Defining an Object
    An object is defined using the `object` keyword. Here is an example of a simple object definition.
    """
  ) {
    object MyObject {
      val greeting: String = "Hello, World!"
      def greet(): String = greeting
    }

    MyObject.greeting should be("Hello, World!")
    MyObject.greet() should be("Hello, World!")
  }

  koan(
    """# Companion Objects
    A companion object is an object with the same name as a class. It is used to define methods and values that are related to the class but are not specific to instances of the class.
    """
  ) {
    class MyClass(val name: String)

    object MyClass {
      def apply(name: String): MyClass = new MyClass(name)
    }

    val instance = MyClass("Scala")
    instance.name should be("Scala")
  }

  koan(
    """# Singleton Objects
    Singleton objects are objects that are instantiated only once. They are useful for defining constants and utility methods.
    """
  ) {
    object Singleton {
      val constant: Int = 42
      def double(x: Int): Int = x * 2
    }

    Singleton.constant should be(42)
    Singleton.double(21) should be(42)
  }

  koan(
    """# Objects with Inheritance
    Objects can extend classes and traits, just like classes. Here is an example of an object extending a trait.
    """
  ) {
    trait Greeter {
      def greet(name: String): String
    }

    object FriendlyGreeter extends Greeter {
      def greet(name: String): String = s"Hello, $name!"
    }

    FriendlyGreeter.greet("Scala") should be("Hello, Scala!")
  }

  codetask(
    """# Exercise: (object with method)
    Create an object `MathUtils` with a method `square` that takes an integer and returns its square.
    """
  ) {
    object MathUtils {
      //solve
      def square(x: Int): Int = x * x
      //endsolve
    }

    //test
    MathUtils.square(3) should be(9)
    MathUtils.square(4) should be(16)
    //endtest
  }

}