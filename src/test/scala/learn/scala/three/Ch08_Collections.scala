package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch08_Collections extends CodeTaskSuite("Collections in Scala", 8) {

  info(
    """# Collections in Scala
    Scala provides a rich set of collection libraries. Collections are containers that hold a number of items, usually of the same type. They can be mutable or immutable.
    """
  )

  koan(
    """# Immutable vs Mutable Collections
    Immutable collections cannot be changed after they are created, while mutable collections can be modified.
    """
  ) {
    val immutableList = List(1, 2, 3)
    val mutableList = scala.collection.mutable.ListBuffer(1, 2, 3)

    // Immutable list cannot be modified
    // immutableList += 4 // This will cause a compilation error

    // Mutable list can be modified
    mutableList += 4
    mutableList should be(scala.collection.mutable.ListBuffer(1, 2, 3, 4))
  }

  koan(
    """# Common Collection Types
    Scala provides various collection types such as List, Set, and Map.
    """
  ) {
    val list = List(1, 2, 3)
    val set = Set(1, 2, 3)
    val map = Map(1 -> "one", 2 -> "two", 3 -> "three")

    list should be(List(1, 2, 3))
    set should be(Set(1, 2, 3))
    map should be(Map(1 -> "one", 2 -> "two", 3 -> "three"))
  }

  koan(
    """# Collection Operations
    Collections support various operations such as map, filter, and reduce.
    """
  ) {
    val list = List(1, 2, 3, 4, 5)

    val mappedList = list.map(_ * 2)
    mappedList should be(List(2, 4, 6, 8, 10))

    val filteredList = list.filter(_ % 2 == 0)
    filteredList should be(List(2, 4))

    val reducedValue = list.reduce(_ + _)
    reducedValue should be(15)
  }
}