package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch03_Println extends CodeTaskSuite("Using println in Scala", 3) {

  info(
    """# Using println
    The `println` method in Scala is used to print text to the console. It is a fundamental tool for debugging and displaying information to the user.

    Let's explore how to use `println` with various data types and expressions.
    """
  )

  video("# println", "H_4DX-D869E")

  singleChoice(
    """# Single Choice Task
    What is the primary use of the `println` method in Scala?
    """
  ) {
    val solutions = List(
      ("To read input from the console", false),
      ("To print text to the console", true),
      ("To perform arithmetic operations", false),
      ("To define variables", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following are valid uses of the `println` method in Scala?
    """
  ) {
    val solutions = List(
      ("Printing a string", true),
      ("Printing the value of a variable", true),
      ("Printing the result of an expression", true),
      ("Defining a function", false)
    )
  }

  singleChoice(
    """# Single Choice Task
    What is the output of the following code?
    ```
    println("Hello, World!")
    ```
    """
  ) {
    val solutions = List(
      ("Hello, World!", true),
      ("Hello World", false),
      ("Hello, Scala!", false),
      ("Hello", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following statements about `println` are true?
    """
  ) {
    val solutions = List(
      ("It can print multiple values concatenated with the `+` operator", true),
      ("It can use string interpolation to include variables and expressions", true),
      ("It can only print strings", false),
      ("It cannot print the result of arithmetic operations", false)
    )
  }

  info(
    """# Info Task
    Remember to use `println` effectively to debug your code and display important information to the user.
    """
  )
}