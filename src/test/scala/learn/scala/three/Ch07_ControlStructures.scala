package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch07_ControlStructures extends CodeTaskSuite("Control Structures in Scala", 7) {

  info(
    """# Control Structures
    Control structures provide a way for programmers to control the flow of a program. They are a fundamental feature of programming languages that let you handle decision-making and looping tasks.

    Scala’s control structures include:
    - for loops and for expressions
    - if/then/else expressions
    - match expressions (pattern matching)
    - try/catch/finally blocks
    - while loops

    Let's explore each of these control structures with examples.
    """
  )
  video("# Expressions", "vK96VNFFuPU&t=3s")
  koan(
    """# for Loops and for Expressions
    For loops provide a way to iterate over a collection to operate on the collection’s elements.
    """
  ) {
    val numbers = List(1, 2, 3, 4, 5)
    for (i <- numbers) do println(i)

    val evenNumbers = for (i <- 1 to 10 if i % 2 == 0) yield i
    evenNumbers should be(Vector(2, 4, 6, 8, 10))
  }

  koan(
    """# if/then/else Expressions
    If/then/else expressions provide a way to make branching decisions.
    """
  ) {
    val number = -5
    val absValue = if number < 0 then -number else number
    absValue should be(5)

    def compare(a: Int, b: Int): Int =
      if a < b then -1
      else if a == b then 0
      else 1
    end compare

    compare(3, 4) should be(-1)
    compare(5, 5) should be(0)
    compare(6, 2) should be(1)
  }

  koan(
    """# match Expressions and Pattern Matching
    Match expressions and pattern matching are a defining feature of Scala.
    """
  ) {
    def isTrue(a: Any): Boolean = a match
      case false | 0 | "" => false
      case _ => true

    isTrue(0) should be(false)
    isTrue("") should be(false)
    isTrue(1) should be(true)
  }

  koan(
    """# try/catch/finally Blocks
    Scala’s try/catch/finally blocks are similar to Java, but the syntax is slightly different.
    """
  ) {
    def toInt(s: String): Option[Int] =
      try
        Some(s.toInt)
      catch
        case e: NumberFormatException => None

    toInt("123") should be(Some(123))
    toInt("abc") should be(None)
  }

  koan(
    """# while Loops
    While loops are rarely used in Scala, but they are available for use when needed.
    """
  ) {
    var i = 0
    val result = new StringBuilder
    while (i < 5) do
      result.append(i)
      i += 1
    result.toString should be("01234")
  }

  codetask(
    """# Exercise: (match expression)
    Write a function that takes a day of the week as a string and returns whether it is a weekday or weekend.
    """
  ) {
    def dayType(day: String): String = {
      //solve
      day match {
        case "Saturday" | "Sunday" => "Weekend"
        case _ => "Weekday"
      }
      //endsolve
    }

    //test
    dayType("Monday") should be("Weekday")
    dayType("Saturday") should be("Weekend")
    //endtest
  }

  koan(
    """# Exercise: (for loop with guards)
    Write a for loop that iterates over the numbers 1 to 20 and prints only the even numbers.
    """
  ) {
    val evenNumbers = for (i <- 1 to 20 if i % 2 == 0) yield i
    evenNumbers should be(Vector(2, 4, 6, 8, 10, 12, 14, 16, 18, 20))
  }


}
