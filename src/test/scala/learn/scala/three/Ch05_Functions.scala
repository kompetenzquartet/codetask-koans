package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch05_Functions extends CodeTaskSuite("Functions in Scala 3", 5) {

  info(
"""# Info Task

Functions are a fundamental part of Scala, a functional programming language. In Scala, functions can be defined using the `def` keyword, and they can consist of a single line or multiple lines forming a code block.

## Function Definition

A simple function definition in Scala looks like this:

```scala
def functionName(parameter1: Type1, parameter2: Type2): ReturnType = {
  // function body
}
```

Functions can also be defined as lambda expressions, which are anonymous functions. Lambda expressions are defined using the `=>` symbol.

## Example

Here is an example of a simple function and a lambda expression:

```scala
def add(x: Int, y: Int): Int = x + y

val multiply = (x: Int, y: Int) => x * y
```

<blockquote style="color: blue;">Example of function definition and lambda expression:</blockquote>

<table style="border: 1px solid black;">
  <tr>
    <th style="background-color: lightgray;">Description</th>
    <th style="background-color: lightgray;">Code</th>
  </tr>
  <tr>
    <td style="color: red;">Function definition</td>
    <td style="color: green;">def add(x: Int, y: Int): Int = x + y</td>
  </tr>
  <tr>
    <td style="color: red;">Lambda expression</td>
    <td style="color: green;">val multiply = (x: Int, y: Int) => x * y</td>
  </tr>
</table>
"""
  )

  codetask(
    """# Code Task
    Create a function that takes two integers and returns their sum.
    """
  ) {
    def sum(x: Int, y: Int): Int = {
      //solve
      x + y
      //endsolve
    }

    //test
    sum(3, 4) should be(7)
    //endtest
  }

  info(
"""# Info Task

Higher-order functions are functions that take other functions as parameters or return functions as results. They are a powerful feature of Scala and enable a functional programming style.

## Example

Here is an example of a higher-order function that takes a function as a parameter:

```scala
def applyFunction(f: (Int, Int) => Int, x: Int, y: Int): Int = f(x, y)
```

<blockquote style="color: blue;">Example of higher-order function:</blockquote>

<table style="border: 1px solid black;">
  <tr>
    <th style="background-color: lightgray;">Description</th>
    <th style="background-color: lightgray;">Code</th>
  </tr>
  <tr>
    <td style="color: red;">Higher-order function</td>
    <td style="color: green;">def applyFunction(f: (Int, Int) => Int, x: Int, y: Int): Int = f(x, y)</td>
  </tr>
</table>
"""
  )

  codetask(
    """# Code Task
    Create a higher-order function that takes a function and two integers, and applies the function to the integers.
    """
  ) {
    def applyFunction(f: (Int, Int) => Int, x: Int, y: Int): Int = {
      //solve
      f(x, y)
      //endsolve
    }

    //test
    val add = (x: Int, y: Int) => x + y
    applyFunction(add, 3, 4) should be(7)
    //endtest
  }

  singleChoice(
    """# Single Choice Task
    Which of the following statements about functions in Scala is true?
    """
  ) {
    val solutions = List(
      ("Functions in Scala cannot return other functions", false),
      ("Functions in Scala can take other functions as parameters", true),
      ("Functions in Scala must always have a return type", false),
      ("Functions in Scala cannot be defined as lambda expressions", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following statements about higher-order functions in Scala are true? (Select all that apply)
    """
  ) {
    val solutions = List(
      ("Higher-order functions can take other functions as parameters", true),
      ("Higher-order functions can return other functions", true),
      ("Higher-order functions cannot take other functions as parameters", false),
      ("Higher-order functions cannot return other functions", false)
    )
  }

  info(
"""# Info Task

Lambda expressions, also known as anonymous functions, are a concise way to define functions in Scala. They are defined using the `=>` symbol.

## Example

Here is an example of a lambda expression:

```scala
val add = (x: Int, y: Int) => x + y
```

<blockquote style="color: blue;">Example of lambda expression:</blockquote>

<table style="border: 1px solid black;">
  <tr>
    <th style="background-color: lightgray;">Description</th>
    <th style="background-color: lightgray;">Code</th>
  </tr>
  <tr>
    <td style="color: red;">Lambda expression</td>
    <td style="color: green;">val add = (x: Int, y: Int) => x + y</td>
  </tr>
</table>
"""
  )

  codetask(
    """# Code Task
    Create a lambda expression that takes two integers and returns their product.
    """
  ) {
    val multiply = (x: Int, y: Int) => {
      //solve
      x * y
      //endsolve
    }

    //test
    multiply(3, 4) should be(12)
    //endtest
  }
}
