package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch10_TraitsAndMixins extends CodeTaskSuite("Traits and Mixins in Scala", 10) {

  info(
    """# Traits in Scala
    Traits are used to share interfaces and fields between classes. They are similar to Java interfaces but can contain concrete methods and fields.
    """
  )

  koan(
    """# Defining and Using Traits
    A trait is defined using the `trait` keyword. Here is an example of a simple trait definition and usage.
    """
  ) {
    trait Greeter {
      def greet(name: String): String = s"Hello, $name!"
    }

    class Person(val name: String) extends Greeter

    val person = new Person("Scala")
    person.greet(person.name) should be("Hello, Scala!")
  }

  koan(
    """# Mixing Traits into Classes
    Traits can be mixed into classes using the `with` keyword.
    """
  ) {
    trait Friendly {
      def greet(): String = "Hi!"
    }

    class Dog extends Friendly

    val dog = new Dog
    dog.greet() should be("Hi!")
  }

  koan(
    """# Trait Linearization and Diamond Problem
    Scala uses a linearization algorithm to resolve the diamond problem. Here is an example.
    """
  ) {
    trait A {
      def message: String = "A"
    }

    trait B extends A {
      override def message: String = "B"
    }

    trait C extends A {
      override def message: String = "C"
    }

    class D extends B with C

    val d = new D
    d.message should be("C")
  }
}