package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch12_FunctionalProgramming extends CodeTaskSuite("Functional Programming in Scala", 12) {

  info(
    """# Functional Programming in Scala
    Functional programming is a programming paradigm that treats computation as the evaluation of mathematical functions and avoids changing state and mutable data. Scala supports functional programming features such as first-class functions, higher-order functions, and immutability.

    Let's explore functional programming in Scala with examples.
    """
  )

  koan(
    """# First-Class Functions
    In Scala, functions are first-class citizens, meaning they can be assigned to variables, passed as arguments, and returned from other functions.
    """
  ) {
    val add = (x: Int, y: Int) => x + y
    val result = add(2, 3)
    result should be(5)
  }

  koan(
    """# Higher-Order Functions
    Higher-order functions are functions that take other functions as parameters or return functions as results.
    """
  ) {
    def applyOperation(x: Int, y: Int, operation: (Int, Int) => Int): Int = operation(x, y)
    val multiply = (x: Int, y: Int) => x * y
    val result = applyOperation(3, 4, multiply)
    result should be(12)
  }

  koan(
    """# Immutability
    Immutability is a core concept in functional programming. Once created, immutable data cannot be changed.
    """
  ) {
    val list = List(1, 2, 3)
    val newList = list :+ 4
    list should be(List(1, 2, 3))
    newList should be(List(1, 2, 3, 4))
  }

  singleChoice(
    """# Single Choice Task
    What is a key characteristic of functional programming?
    """
  ) {
    val solutions = List(
      ("It relies on mutable state", false),
      ("It treats functions as first-class citizens", true),
      ("It does not support higher-order functions", false),
      ("It encourages side effects", false)
    )
  }

  multipleChoice(
    """# Multiple Choice Task
    Which of the following are features of functional programming in Scala?
    """
  ) {
    val solutions = List(
      ("First-class functions", true),
      ("Higher-order functions", true),
      ("Immutability", true),
      ("Mutable state", false)
    )
  }

  codetask(
    """# Exercise: (function composition)
    Create a function `compose` that takes two functions `f` and `g` and returns a new function that is the composition of `f` and `g`.
    """
  ) {
    def compose[A, B, C](f: B => C, g: A => B): A => C = {
      //solve
      (x: A) => f(g(x))
      //endsolve
    }

    //test
    val f = (x: Int) => x + 1
    val g = (x: Int) => x * 2
    val h = compose(f, g)
    h(3) should be(7)
    h(4) should be(9)
    //endtest
  }

}