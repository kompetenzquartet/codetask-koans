package learn.scala.three

import org.codetask.koanlib.CodeTaskSuite

class Ch04_converting_between_numbers extends CodeTaskSuite("Converting between Numbers", 4) {
  info(
"""# Info Task

## Converting Between Numeric Types

Scala provides methods to convert between different numeric types. Here are some examples:

```scala
val b: Byte = 1
b.toInt         // Int = 1
b.toDouble      // Double = 1.0

val d = 100.0
d.toInt         // Int = 100
d.toByte        // Byte = 100
```
"""
  )

  koan(
    """# Numeric Type Conversion Tests
    Test the conversion between different numeric types.
    """
  ) {
    // Byte to Int
    val b: Byte = 1
    b.toInt should be(1)

    // Double to Int
    val d = 100.0
    d.toInt should be(100)

    // Validity checks
    val dMax = Double.MaxValue
    dMax.isValidInt should be(false)

    val dValid: Double = 65_535.0
    dValid.isValidInt should be(true)

    val dFractional = 1.5
    dFractional.isValidInt should be(false)
  }
}
