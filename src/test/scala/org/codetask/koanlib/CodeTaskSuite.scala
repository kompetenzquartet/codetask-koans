package org.codetask.koanlib

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class CodeTaskSuite(title: String, rank: Long)
    extends AnyFunSuite
    with Matchers {
  def info(description: String) = ()
  def reflection(description: String) = ()
  def singleChoice(name: String)(f: => Unit) = test(name.stripMargin('|'))(f)
  def multipleChoice(name: String)(f: => Unit) = test(name.stripMargin('|'))(f)
  def video(description: String, url: String) = ()
  def koan(name: String)(f: => Unit) = test(name.stripMargin('|'))(f)
  def codetask(name: String)(f: => Unit) = test(name.stripMargin('|'))(f)
}
