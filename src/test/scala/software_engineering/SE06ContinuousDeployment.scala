package software_engineering

import org.codetask.koanlib.CodeTaskSuite

class SE06ContinuousDeployment extends CodeTaskSuite("SE-06-Continuous-Deployment", 5) {

  val methods = "Waterfall, Agile"
  koan("# Value delivered.") {
    // Which are the two methods?
    methods should be("Waterfall, Agile") // "..., ..."

  }

}
