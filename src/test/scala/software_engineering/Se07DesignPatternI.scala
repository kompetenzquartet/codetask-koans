package software_engineering

import org.codetask.koanlib.CodeTaskSuite

class Se07DesignPatternI extends CodeTaskSuite("SE-07-Design-Pattern-I", 6) {

  val singletonDefinition = 2
  koan("# Singleton.") {
    /* Possible definitions:
     * 1: Ensure a class has more than one instance.
     * 2: Ensure a class only has one instance.
     */

    // Fill in the number
    singletonDefinition should be (2)
  }

  val strategyDefinition = 1
  koan("# Strategy.") {
    /* Possible definitions:
     * 1: A family of algorithms, that are interchangeable.
     * 2: A family of algorithms, that are not interchangeable.
     */

    // Fill in the number
    strategyDefinition should be (1)
  }

  val stateDefinition = 2
  koan("# State.") {
    /* Possible definitions:
     * 1: Prevent an Object to alter its behaviour when its internal state changes.
     * 2: Allow an Object to alter its behaviour when its internal state changes.
     */

    // Fill in the number
    stateDefinition should be (2)
  }

  val s = "single responsibility principle"
  val o = "open"
  val l = "liskov substitution principle"
  val i = "interface segregation principle"
  val d = "dependency inversion principle"
  koan("# Design Pattern.") {
    // What does SOLID mean?
    s should be("Single Responsibility Principle".toLowerCase())
    o should be("Open".toLowerCase())
    l should be("Liskov Substitution Principle".toLowerCase())
    i should be("Interface Segregation Principle".toLowerCase())
    d should be("Dependency Inversion Principle".toLowerCase())
  }

}
