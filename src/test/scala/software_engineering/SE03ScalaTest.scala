package software_engineering

import org.codetask.koanlib.CodeTaskSuite

class SE03ScalaTest extends CodeTaskSuite("SE-03-Scala-Test", 3) {

  koan("# Possible ways of testing equality using the example of a method.") {
    def sum(x: Int, y: Int): Int = {
      x + y
    }

    // There are different ways to check equality
    sum(2, 4) should be(6)
    sum(2, 4) should equal(6)
    sum(2, 4) shouldEqual (6)
    sum(2, 4) should ===(6)
    sum(2, 4) shouldBe(6)
  }

  val testResult = true
  val testresult = false
  koan("# Testing an Array.") {
    // result = Array(1, 2) should equal(Array(1, 2))
    testResult should be(true)
  }

  koan("# Testing Strings. (1)") {
    val string = "Hello World, this is Sparta!"

    // Testing if the string starts with 'Hello'
    /* string should startWith('Hello') */
    testResult should be(true)

    // Using regex to test
    /* string should include('this is') */
    string should startWith regex "Hel*o"

    // Testing if the string ends with 'Sparta!'
    /* string should endWith('Sparta!) */
    testResult should be(true)

    // Fill in a string that delivers a positive test result
    string should include("this is")

    // Testing if the string contains 'thiss'
    /* string should include('this is') */
    testresult should be(false)

    // Testing if the string meets the regex conditions
    /* "abbccxxx" should startWith regex ("a(b*)(c*)" withGroups("bb", "cc")) */
    testResult should be(true)

    // Testing if the string fully meets the regex conditions
    /* "abbcc" should fullyMatch regex ("a(b*)(c*)" withGroups("aa", "cc")) */
    testresult should be(false)

    // Testing if the string is empty
    /* string shouldBe empty */
    testresult should be(false)
  }

  koan("# Testing Strings. (2)") {
    val otherString = "Change is good, but dollars are better."

    // Fill in a string that delivers a positive test result
    otherString should startWith("Change")
    otherString should endWith("better.")
    otherString should include("dollars")
  }

  koan("# Testing Objects.") {

    object ReturnTrue {
      def methodTrue(): Boolean = {
        true
      }
    }

    object ReturnFalse {
      def methodFalse(): Boolean = {
        false
      }
    }

    // Testing if the return value of the method from the object 'ReturnTrue' is true
    /* ReturnTrue shouldBe 'methodTrue <--- pay attention to the quote sign */
    testResult should be(true)

    // Testing if the return value of the method from the object 'ReturnFalse' is false
    /* ReturnFalse should not be 'methodFalse */
    testResult should be(true)
  }

  koan("# Testing Range.") {
    val point = 4.8

    // Test if the value of point is within the range
    /* point should equal(5.0 +- 0.1) */
    testresult should be(false)

    // Test if the value of point is within the range
    /* point should equal(5.0 +- 0.2) */
    testResult should be(true)
  }

  koan("# Testing an empty Object.") {

    // None is an empty Scala object
    /* None shouldBe empty */
    testResult should be(true)
  }
}
