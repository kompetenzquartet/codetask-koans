package software_engineering

import org.codetask.koanlib.CodeTaskSuite

import scala.collection.mutable

class SE04MoreScala extends CodeTaskSuite("SE-04-More-Scala", 4) {

  val nsrf = true
  koan("# Null is a subtype of any Reference Type.") {
    nsrf should be(true)
  }

  val tc = false
  koan("# Null is NOT type compatible with any Reference Type.") {
    tc should be(false)
  }

  val map = Map("key1" -> 10, "key2" -> 20, "key3" -> 30)
  var set: mutable.Set[Int] = mutable.Set(1, 2, 3)
  val remove2: Boolean = set.remove(2)
  val add4: Boolean = set.add(4)
  koan("# Collections - Map, Iterable, Set.") {
    // Create a Map | keys: "key1", "key2", "key3" | values: 10, 20, 30
    map should be(Map("key1" -> 10, "key2" -> 20, "key3" -> 30))

    val it = Iterable("a", "b", "c")
    it.head should be("a")

    val set = mutable.Set(1, 2, 3)
    // Remove the 2 and add a 4 at the end
    remove2 should be(set.remove(2)) // alternative: set -= 2
    add4 should be(set.add(4)) // alternative: set += 4
  }

  val output1 = List(1, 2, 3, 4, 5, 6)
  val output2 = List(1, 2, 3, 4)
  koan("# List.") {
    val list = List(1, 2, 3)
    println(list.head) // Output: 1
    println(list.tail) // Output: List(2, 3)
    list.isEmpty should be(false)

    println(List(1, 2, 3) ::: List(4, 5, 6))
    output1 should be(List(1, 2, 3, 4, 5, 6))

    println(1 :: 2 :: 3 :: 4 :: Nil)
    output2 should be(List(1, 2, 3, 4))
  }

  val seol = true
  koan("# Scala is an expression-oriented language.") {
    seol should be(true)
  }

  val se = false
  koan("# Expressions always have side-effects, statements usually don‘t.") {
    se should be(false)
  }

  val lastNumber = 3
  val number = 4
  val output = 3
  koan("# For Comprehension.") {
    val nums = Seq(1, 2, 3)
    val letters = Seq('a', 'b', 'c')
    val res = for {
      n <- nums
      c <- letters
    } yield (n, c)

    // What is the last number that is printed?
    for (i <- 1 to 10 if i < 4) println(i)
    lastNumber should be(3)

    // Choose the right output
    for (i <- 2 to 10 by 2 if i > 5) println(i)
    /* Options
     * 1: 2, 4, 6, 8, 10
     * 2: 7, 9
     * 3: 6, 8, 10
     * 4: 1, 3
     */
    output should be(3)

    // Which number is printed?
    for {
      i <- 1 to 10
      if i > 3
      if i < 6
      if i % 2 == 0
    } println(i)
    number should be(4)
  }

  val caseName = "p"
  koan("# Match - Expression.") {

    /* Example */
    val i = 5
    i match {
      case 1 => println("January")
      case 2 => println("February")
      case 3 => println("March")
      case 4 => println("April")
      case 5 => println("May") // Is printed
      case 6 => println("June")
      case 7 => println("July")
      case 8 => println("August")
      case 9 => println("September")
      case 10 => println("October")
      case 11 => println("November")
      case 12 => println("December")
      // Default case
      case _ => println("Invalid month")
    }
    /* Example End */

    def describe(x: Int) =
      x match {
        case n: Int if (n < 0) => n + " is negative"
        case z: Int if (z == 0) => z + " is zero"
        case p: Int if (p > 0) => p + " is positive"
      }
    // Which case matches?
    describe(3)
    caseName should be("p") // " "
  }

  val printed = "starting"
  koan("# Match - Handling alternate cases.") {
    def isTrue(a: Any) = a match { // The "match"-expressions let you handle multiple cases in a single case statement.
      case 0 | "" => false
      case _ => true
    }

    val i = 4
    val evenOrOdd = i match {
      case 1 | 3 | 5 | 7 | 9 => println("odd")
      case 2 | 4 | 6 | 8 | 10 => println("even")
      case _ => println("some other number")
    }

    val cmd = "go"
    cmd match {
      case "start" | "go" => println("starting")
      case "stop" | "quit" | "exit" => println("stopping")
      case _ => println("doing nothing")
    }
    // What is going to be printed?x
    printed should be("starting")
  }

  val count = 200
  val case100 = "case x if x >= 100 =>"
  koan("# Match - Using if expressions in case statements.") {
    count match {
      case 1 => println("one, a lonely number")
      case x if x == 2 || x == 3 => println("two's company, three's a crowd")
      case x if x > 3 & x < 100 => println("not the biggest party i've ever been to")
      case _ => println("i'm guessing your number is zero or less")
    }
    // Add a case for when the number is greater than or equal 100 | use x

    case100 + println("100+, that's a party")
    case100 should be("case x if x >= 100 =>")

  }

  abstract class Notification

  case class Email(sender: String, title: String, body: String) extends Notification

  case class SMS(caller: String, message: String) extends Notification

  case class VoiceRecording(contactName: String, link: String) extends Notification

  def showNotification(notification: Notification): String = {
    notification match {
      case Email(sender, title, _) =>
        s"You got an email from $sender with title: $title"
      case SMS(number, message) =>
        s"You got an SMS from $number! Message: $message"
      case VoiceRecording(name, link) =>
        s"You received a Voice Recording from $name! Click the link to hear it: $link"
    }
  }

  val someSms = SMS("12345", "Are you there?")
  val someVoiceRecording = VoiceRecording("Tom", "voicerecording.org/id/123")
  val smscall = showNotification(someSms)
  val voicecall = showNotification(someVoiceRecording)
  koan("# Match.") {
    abstract class Notification
    case class Email(sender: String, title: String, body: String) extends Notification
    case class SMS(caller: String, message: String) extends Notification
    case class VoiceRecording(contactName: String, link: String) extends Notification

    def showNotification(notification: Notification): String = {
      notification match {
        case Email(sender, title, _) =>
          s"You got an email from $sender with title: $title"
        case SMS(number, message) =>
          s"You got an SMS from $number! Message: $message"
        case VoiceRecording(name, link) =>
          s"You received a Voice Recording from $name! Click the link to hear it: $link"
      }
    }

    val someSms = SMS("12345", "Are you there?")
    val someVoiceRecording = VoiceRecording("Tom", "voicerecording.org/id/123")

    // Call the function with someSms as parameter
    smscall should be(showNotification(someSms)) // "You got an SMS from 12345! Message: Are you there?"

    // Call the function with someVoiceRecording as parameter
    voicecall should be(showNotification(someVoiceRecording)) // "You received a Voice Recording from Tom! Click the link to hear it: voicerecording.org/id/123"
  }
}
