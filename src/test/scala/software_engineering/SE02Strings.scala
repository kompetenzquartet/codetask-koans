package software_engineering

import org.codetask.koanlib.CodeTaskSuite

class SE02Strings extends CodeTaskSuite("SE-02-String-Handling", 2) {

  val str1 = "I stepped on a Corn Flake, "
  val str2 = "now I'm a Cereal Killer."
  val str: String = str1 + str2
  koan("# String Concatenation. (1)") {
    val str1 = "I stepped on a Corn Flake, "
    val str2 = "now I'm a Cereal Killer."

    // Combine the values str1 and str2 to a full sentence
    str should be(str1 + str2) // Tip: use +
  }

  val con: String = "I stepped on " + "your " + "Corn Flakes."
  koan("# String Concatenation. (2)") {
    /* Combine the following string pieces to a full sentence with +
      * "I stepped on "
      * "your "
      * "Corn Flakes."
      * Output: I stepped on your Corn Flakes.
     */
    con should be("I stepped on " + "your " + "Corn Flakes.")
  }

  val myNumber = 6.5
  val concatenation: String = "My number is: " + myNumber
  koan("# String Concatenation - different data types. (1)") {
    val myNumber = 6.5

    /* Combine the following pieces to a full sentence with +
      * "My number is: "
      * val myNumber
      * Output: My number is: 6.5
     */
    concatenation should be("My number is: " + myNumber)
  }

  val b = 32
  val str3 = "now I'm a Cereal Killer."
  val combined: String = "I stepped on " + b + " Corn Flakes, " + str3
  koan("# String Concatenation - different data types. (2)") {
    val number = 32
    val string = "now I'm a Cereal Killer."

    /* Combine the following pieces to a full sentence with +
      * "I stepped on "
      * val number
      * " Corn Flakes, "
      * val string
      * Output: I stepped on 32 Corn Flakes, now I'm a Cereal Killer.
     */
    combined should be("I stepped on " + number + " Corn Flakes, " + string)
  }

  koan("# String Concatenation - if-Expression. (1)") {
    val b = true
    val con1 = "a" + (if (b) "b" else "") + "c"

    con1 should be("abc")

    val c = false
    val con2 = "a" + "b" + (if (c) "c" else "")

    con2 should be("ab")

    val d = false
    val con3 = "a" + "b" + (if (d) "d" else "c")

    con3 should be("abc")
  }

  koan("# String Concatenation - if-Expression. (2)") {
    val time = 10
    val con4 = "See you in " + (if (time == 10) time + "min" else "never")

    con4 should be("See you in 10min")

    val animal = "dog"
    val con5 = "The sound is: " + (if (animal == "cat") "meow" else "wau")

    con5 should be("The sound is: wau")
  }

  koan("# String Concatenation - List and for-Loop.") {
    val list1: List[String] = List("a", "b", "c")
    // or
    val list2 = List("a", "b", "c")

    var con1 = ""
    for (a <- list1) {
      con1 += a
    }
    con1 should be("abc")

    val intList = List(1, 2, 3, 4, 5) // or val intList = Array(1, 2, 3, 4, 5)
    var con2 = 0
    for (a <- intList) {
      con2 += a
    }
    con2 should be(15)
  }

  koan("# String Concatenation - foreach-Loop and if-Expression.") {
    /*Examples Start*/

    val exampleList = List(1, "hallway ", 2, "soapboxes ", 1, "race")
    var all = ""
    exampleList.foreach (
      element => all += element
    )
    println(all) // Output: 1hallway 2soapboxes 1race

    var strings = ""
    exampleList.foreach (
      element => (if (element.isInstanceOf[String]) strings += element)
    )
    println(strings) // Output: hallway soapboxes race

    var integer = ""
    exampleList.foreach (
      element => (if (element.isInstanceOf[Int]) integer += element)
    )
    println(integer) // Output: 121

    /*Examples End*/

    val list = List("I can eat ", 23, " hot dogs.")
    var result = ""
    list.foreach (
      element => result += element
    )
    result should be ("I can eat 23 hot dogs.")
  }

  koan("# String Concatenation - Multiline Strings") {
    /*Examples Start*/

    val a =
      """Stepping on Corn Flakes
        is a disaster."""
    /*
     * Stepping on Corn Flakes
     __________is a disaster.
     * The second line will end up with whitespace at the beginning
     */

    val b =
      """Stepping on Corn Flakes
        |is a disaster.""".stripMargin
    /*
     * Stepping on Corn Flakes
       is a disaster.
     */

    val c =
      """Stepping on Corn Flakes
        #is a disaster.""".stripMargin('#')
    /*
     * Stepping on Corn Flakes
       is a disaster.
     */

    /*Examples End*/
  }

  val missingExpression: String = "stripMargin"
  koan("# String Concatenation - List and Multiline Strings") {
    val list = List("I have a ", "soapbox and I ", "drove down the hallway.")

    var con = ""
    for (a <- list) {
      con += "|" + a + "\n"
    }
    /* What is missing to get a correct multiline string?
     * con.missingExpression
     * Output:
     * I have a
     * soapbox and I
     * drove down the hallway.
    */
    missingExpression should be ("stripMargin") // insert the Expression as a string ""
  }

  val number = 54
  val weight = 82.6
  val text = "there are only crumbles left"
  val string = s"I stepped on $number Corn Flakes and I weigh $weight kg so $text."
  koan("# String Interpolation - s Interpolator.") {
    /*Example Start*/
    val floatVal = 12.456
    val intVal = 2000
    val stringVal = "Hello, Scala!"

    println(s"The value of the float variable is $floatVal while the value of the integer variable is $intVal and the string is $stringVal")
    // Output: The value of the float variable is 12.456 while the value of the integer variable is 2000 and the string is Hello, Scala!
    // INFO: Prepending s to any string literal allows the usage of variables directly in the string.
    /*Example End*/

    val number = 54
    val weight = 82.6
    val text = "there are only crumbles left"

    // Construct the sentence with the values as shown above (without println) | use $
    // "I stepped on 54 Corn Flakes and I weigh 82.6 kg so there are only crumbles left." | Dont' forget the dot at the end!!
    string should be(s"I stepped on $number Corn Flakes and I weigh $weight kg so $text.")
  }

  val string1 = "hallway"
  val size1 = 54
  val sentence: String = s"I drove down the " + string1 + s" with $size1 km/h in my soapbox."
  koan("# String Concatenation and Interpolation. (1)") {
    val string = "hallway"
    val size = 54

    /* Combine the values to a full sentence, use + and $
     * "I drove down the hallway with 54 km/h in my soapbox."
     * Pay attention to the blanks
     */
    sentence should be(s"I drove down the " + string + s" with $size km/h in my soapbox.") // Tip: before every starting string has to be an s".."
  }

  val string2 = "I can "
  val size2 = 23
  val sentence2: String = string2 + s"eat $size2 hot dogs."
  koan("# String Concatenation and Interpolation. (2)") {
    val string = "I can "
    val size = 23

    // Combine  the values to a full sentence, use + and $
    // "I can eat 23 hot dogs."
    // Pay attention to the blanks
    sentence2 should be(string + s"eat $size hot dogs.")
  }
}
