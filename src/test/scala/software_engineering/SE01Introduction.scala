package software_engineering

import org.codetask.koanlib.CodeTaskSuite

class SE01Introduction extends CodeTaskSuite("SE-01-Introduction", 1) {

  koan("# vals may not be reassigned unlike var") {
    val a = 5
    a should be(5)
    /*
       a is immutable
       a = 7  would cause a compilation problem
     */

    var b = 'B'
    b.toString should be("B")
    b = 'b'
    b.toString should be("b")
  }

  koan("# Checking the return value of this function.") {
    def larger(x: Int, y: Int): Boolean = x < y

    larger(1, 2) should be(true)
  }

  val return_type = "Int"
  koan("# What is the return type of this function.") {
    def addInt(a: Int, b: Int): Int = {
      var sum: Int = 0
      sum = a + b
      return sum
    }

    return_type should be("Int") // put the answer in quotes: "answer"
  }

  def functionToAdd(a: Int, b: Int): Int = {
    var sum: Int = 0
    sum = a + b
    return sum
  }
  val function_call: Int = functionToAdd(5, 3)
  koan("# How to call a function.") {

    // declaration and definition of function
    def functionToAdd(a: Int, b: Int): Int = {
      var sum: Int = 0
      sum = a + b
      return sum
    }

    object Example {
      def main(args: Array[String]) =
        // Calling the function "functionToAdd" with the parameters 5 and 3
        function_call should be(functionToAdd(5, 3))

    }
  }

  val function_name = "buyDoughnuts"
  val parameter_type = "Int"
  val returnType = "Int"
  val functionCall = "buyDoughnuts(4, 2)"
  koan("# Fill in the gaps with the given values.") {
    def buyDoughnuts(persons: Int, doughnutsPerPerson: Int): Int = {
      val doughnuts = persons * doughnutsPerPerson
      return doughnuts
    }

    buyDoughnuts(4, 2)

    // surround all the answers with quotes ""
    function_name should be("buyDoughnuts")
    parameter_type should be("Int")
    returnType should be("Int")
    functionCall should be(
      "buyDoughnuts(4, 2)"
    ) // Fill in the whole function call
  }

  koan("# Scala classes can have parameters.") {
    class Complex(real: Double, imaginary: Double) {
      def re: Double = real

      def im(): Double = imaginary
    }

    val c = new Complex(1.5, 2.3)
    c.im() should be(2.3)
    c.re should be(1.5)
  }

  val number_of_methods = 2
  koan("# How many methods does this class have?") {
    class Point(var x: Int, var y: Int) {

      def move(dx: Int, dy: Int): Unit = {
        x = x + dx
        y = y + dy
      }

      override def toString: String = s"($x, $y)"
    }

    number_of_methods should be(2)
  }

  koan("# If-Else Expression within a value.") {
    val x = 10
    val y = 20

    val max = if (x > y) x else y
    max should be(20)

  }

  koan("# If-Else Expression within a function.") {
    def min(x: Int, y: Int): Int = {
      if (x < y)
        x
      else
        y
    }

    min(7, 4) should be(4)
  }

  val counter = 8
  koan("# The for-loop (1).") {
    val numList = List(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    // for loop execution with multiple filters
    for (a <- numList; if a < 8) {
      println("Value of a: " + a);
    }
    // How many times is the loop executed
    counter should be(8)
  }

  val a_in_third_pass = 4
  koan("# The for-loop (2).") {
    val numList = List(1, 2, 3, 4, 5);
    // for loop execution with multiple filters
    for (a <- numList if a != 3; if a < 5) {
      println("Value of a: " + a);
    }
    // Which value does a have in the third pass?
    a_in_third_pass should be(4)
  }

  koan("# A simple class in Scala.") {
    class Person(var name: String, var age: Int)
    val al = new Person("Al", 42)
    al.name should be("Al")
    al.age should be(42)
  }

  val new_instance = "new Point(2, 3)"
  koan("# Create an instance of the class Point.") {
    class Point(var x: Int, var y: Int) {

      def move(dx: Int, dy: Int): Unit = {
        x = x + dx
        y = y + dy
      }

      override def toString: String = s"($x, $y)"
    }

    // Create an instance of Point with the parameters 2 and 3
    // put the answer in quotes: "answer" (normally it would be without quotes)
    val point1 = new_instance
    new_instance should be("new Point(2, 3)")
  }

  koan(
    "# When you define a class as a case class, you don’t have to use the 'new' keyword to create a new instance."
  ) {
    case class Message(sender: String, recipient: String, body: String)
    val message1 = Message("Anna", "Jorge", "Ça va ?")

    message1.sender should be("Anna")
  }

  koan("# Case classes (1).") {
    case class Cell(value: Int) {
      def isSet: Boolean = value != 0
    }

    val cell1 = Cell(2)
    cell1.isSet should be(true)

    val cell2 = Cell(0)
    cell2.isSet should be(false)

    case class Field(cells: Array[Cell])

    val field1 = Field(Array.ofDim[Cell](1))
    field1.cells(0) = cell1

    case class House(cells: Vector[Cell])

    val house = House(Vector(cell1, cell2))

    house.cells(0).value should be(2)
    house.cells(0).isSet should be(true)
  }

  koan("# Case classes (2).") {
    case class Car(value: Int) {
      def isSet: Boolean = value != 0
    }

    val car1 = Car(0)
    car1.isSet should be(false)

    val car2 = Car(12)
    car2.isSet should be(true)

    case class Field(cars: Array[Car])

    val field1 = Field(Array.ofDim[Car](2))
    field1.cars(0) = car1
    field1.cars(1) = car2

    case class Garage(cars: Vector[Car])

    val house = Garage(Vector(car1, car2))

    house.cars(0).isSet should be(false)
    house.cars(1).value should be(12)
  }

  koan("""# Here we create our own prefix operator for our own class.
      The only identifiers that can be used as prefix operators are 
      +, -, !, and ~""") {
    class Stereo {
      def unary_+ = "on"

      def unary_- = "off"
    }

    val stereo = new Stereo
    (+stereo) should be("on")
    (-stereo) should be("off")
  }

  val scala_isool = true
  koan("# Scala is an Object-Oriented Language.") {
    scala_isool should be(true)
  }

  val scala_isfl = true
  koan("# Scala is a Functional Language.") {
    scala_isfl should be(true)
  }

  val scala_ijc = true
  koan("# Scala classes can inherit from Java classes.") {
    scala_ijc should be(true)
  }

  val java_csc = false
  koan("# Java classes can call all Scala classes without fail.") {
    java_csc should be(false)
  }

  val numbers_are_objects = true
  koan("# Numbers are objects.") {
    numbers_are_objects should be(true)
  }

  val are_objects = true
  koan("# 3, 1.7, “Hello World“, (0,1,2) are objects.") {
    are_objects should be(true)
  }

  val answer = false
  koan("# Immutable values start with keyword var.") {
    answer should be(false)
  }
}
