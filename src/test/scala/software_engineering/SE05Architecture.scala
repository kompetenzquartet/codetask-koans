package software_engineering

import org.codetask.koanlib.CodeTaskSuite

class SE05Architecture extends CodeTaskSuite("SE-05-Architecture", 5) {

  val rightAnswer = 1
  koan("# What is Cyclomatic Complexity?") {
    /* Possible answers:
     * 1: It is a quantitative measure of the number of linearly independent paths through a program's source code.
     * 2: It is a quantitative measure of the number of all possible linearly paths through a program's source code.
     */

    // Fill in the number
    rightAnswer should be(1)
  }

  val cyco = 3
  koan("# Cyclomatic Complexity. (1)") {
    val E = 5
    val N = 4
    // Calculate the Cyclomatic Complexity
    cyco should be(3)
  }

  val cycom = 5
  koan("# Cyclomatic Complexity. (2)") {
    /* The graph that represents the code includes
     * 6 conditions
     * 7 statements
     * 16 Nodes
     * Calculate the Cyclomatic Complexity.
     */

    cycom should be(5)
  }

  val condition1, condition2 = true
  val statement1, statement2, statement3, statement4, statement5 = true
  val cc = 4
  koan("# Cyclomatic Complexity. (3)") {
    if (condition1) {
      if (condition2)
        statement1
      else
        statement2
      for (x <- 1 to 10) { // condition3 | Hint: This is a loop
        statement3
      }
    } else {
      statement4
    }
    statement5

    // What is the Cyclomatic Complexity of this Code?
    cc should be(4)
  }

  val layer1 = "ui layer"
  var layer2 = "business layer"
  val layer3 = "data-layer"
  koan("# Layered Architecture.") {
    // What are the layers of the 3 layered model? (According to the lecture.)
    // Starting with from the top | Same spelling as in the lecture.
    layer1 should be("UI layer".toLowerCase())
    layer2 should be("Business layer".toLowerCase())
    layer3 should be("Data-layer".toLowerCase())
  }

  val observer = "update"
  val observable = "add, remove, notify"
  koan("# Observer and Observable.") {
    // What are the functions of Observer and Observable?
    observer should be("update") // "..."
    observable should be("add, remove, notify") // "..."
  }

  val model = 2
  val view = 14
  val controller = 3
  koan("# MVC.") {
    // Assign the functions to the suitable components.
    val function1 = "Presenting the data."
    val function2 = "Containing the data."
    val function3 = "Adapting the data."
    val function4 = "Realisation of user interaction."

    // Only fill in the numbers | e.g. if the model matches function1 and function2 write 12
    model should be(2)
    view should be(14)
    controller should be(3)
  }
}
